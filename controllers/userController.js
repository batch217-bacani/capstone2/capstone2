const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


module.exports.registerUser = (data) => {
	let newUser = new User ({
		firstName : data.firstName,
		lastName : data.lastName,
		email : data.email,
		mobileNo : data.mobileNo,
		password : bcrypt.hashSync(data.password, 10),
		isAdmin : data.isAdmin
	})

	return newUser.save().then((user, error)=> {
		if(error){
			return "Failed to register, try again."
		}else{
			return "You've successfully registered."
		}
	});
};

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				// Generate an access token
				return {access : auth.createAccessToken(result)}
			}
		}
	})
}

/*module.exports.loginUser = (data) => {
	return User.findOne({email: data.email}).then(result => {
		if(result == null){
			return "User not found, Please register first.";
		}else{
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

			if(isPasswordCorrect){
				let access = auth.createAccessToken(result);
				return `Hi ${result.firstName}! Welcome to Bacani's Camera Shop, Enjoy Shopping!`;
			}else{
				return "Email and Password does not match, Please try again."
			}
		}
	});
};
*/

