const express = require("express");
const router = express.Router();
const Product = require("../models/Product.js")
const productController = require("../controllers/productController.js")
const auth = require("../auth.js")


router.post("/create", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.addProduct(req.body, isAdmin)
	.then(resultFromController => res.send(resultFromController));
});

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => {
		res.send(resultFromController)
	})
})


router.get("/active", (req, res) => {
	productController.getActiveProducts(req.params)
	.then(resultFromController => res.send(resultFromController));
});


router.get("/:productId", (req, res) => {
	productController.getProductDetails(req.params.productId)
	.then(resultFromController => res.send(resultFromController));
});


router.put("/update/:productId", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params.productId, isAdmin, req.body)
	.then(resultFromController => res.send(resultFromController));
});


router.put("/archive/:productId", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params.productId, isAdmin)
	.then(resultFromController => res.send(resultFromController));
});


module.exports = router;
