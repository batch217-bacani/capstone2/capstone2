const express = require("express");
const router = express.Router();
const User = require("../models/User.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")

router.post("/register", (req, res) => {
	userController.registerUser(req.body)
	.then(resultFromController => res.send(resultFromController));
});


router.post("/login", (req, res) => {
	userController.loginUser(req.body)
	.then(resultFromController => res.send(resultFromController));
});


router.post("/checkout", auth.verify , (req, res) => {
	let customer = {
		userId: auth.decode(req.headers.authorization).id,
		fullName: auth.decode(req.headers.authorization).fullName,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	orderController.createOrder(customer, req.body)
	.then(resultFromController => res.send(resultFromController));
});


router.get("/userDetails/:userId", auth.verify, (req, res) => {
	userController.getUserDetails(req.params.userId)
	.then(resultFromController => res.send(resultFromController));
});


router.put("/setAsAdmin/:newAdminUserId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin

	userController.addAdmin(isAdmin, req.params.newAdminUserId)
	.then(resultFromController => res.send(resultFromController));
});


router.get("/myOrders", auth.verify, (req, res) => {
	let customer = {
		userId: auth.decode(req.headers.authorization).id,
		fullName: auth.decode(req.headers.authorization).fullName,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	orderController.getMyOrders(customer)
	.then(resultFromController => res.send(resultFromController));
});


router.get("/orders", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;

	orderController.getAllOrders(isAdmin)
	.then(resultFromController => res.send(resultFromController));
});


router.post("/addToCart", auth.verify, (req, res) => {
	let customer = {
		userId: auth.decode(req.headers.authorization).id,
		fullName: auth.decode(req.headers.authorization).fullName,
		isAdmin: auth.decode(req.headers.authorization).isAdmin	
	}

	cartController.addToCart(customer, request.body)
	.then(resultFromController => response.send(resultFromController));
});



module.exports = router;
