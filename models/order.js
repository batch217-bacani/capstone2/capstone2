const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	products: [{
		productId: {
			type: String,
			required: [true, "Product ID is required"]
		},
		productName: {
			type: String,
			required: [true, "Product Name is required."]
		},

		productPrice: {
			type: Number,
			required: [true, "Product Price is required."]
		},

		quantity: {
			type: Number,
			default: 1
		},

	}],
	
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"]
	},
	purchaseOn: {
		type: Date,
		default: new Date()
	}
})
